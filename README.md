# Read Me First
The following was discovered as part of building this project:

* The original package name 'com.hyperglance.web-crawler' is invalid and this project uses 'com.hyperglance.webcrawler' instead.

# Getting Started

Steps in running:
1. Run as spring boot
2. Use a postman to run the crawler

   Note: Take note the inventory-id from the response

  ```
   POST http://localhost:8080/crawl
   {
       "url" : "http://google.com",
       "depth" : "3"
   }
   Response:
   crawling spawned
  ```
3. Check the inventory. Use postman as well to get it.
    It will result the root url used and dept. Then the crawled urls together with the list of parsedImages
    
    Used the entryId
    
  ```
GET http://localhost:8080/inventory

[
    {
        "url": "http://google.com",
        "images": [
            {
                "image": "https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png"
            }
        ]
    },
    {
        "url": "https://www.google.com.ph/imghp?hl=en&tab=wi&ogbl",
        "images": []
    }
]
      
```
## Reference Documentation
For further reference, please consider the following sections:

* [Official Gradle documentation](https://docs.gradle.org)
* [Spring Boot Gradle Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.4.0/gradle-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.4.0/gradle-plugin/reference/html/#build-image)
* [Jersey](https://docs.spring.io/spring-boot/docs/2.4.0/reference/htmlsingle/#boot-features-jersey)
* [Spring Data JPA](https://docs.spring.io/spring-boot/docs/2.4.0/reference/htmlsingle/#boot-features-jpa-and-spring-data)

### Guides
The following guides illustrate how to use some features concretely:

* [Accessing Data with JPA](https://spring.io/guides/gs/accessing-data-jpa/)

### Additional Links
These additional references should also help you:

* [Gradle Build Scans – insights for your project's build](https://scans.gradle.com#gradle)