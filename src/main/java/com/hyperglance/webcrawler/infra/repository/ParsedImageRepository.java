package com.hyperglance.webcrawler.infra.repository;

import com.hyperglance.webcrawler.infra.repository.entity.ParsedImageEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ParsedImageRepository extends JpaRepository<ParsedImageEntity, Long> {
}
