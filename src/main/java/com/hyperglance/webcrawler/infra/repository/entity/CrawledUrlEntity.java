package com.hyperglance.webcrawler.infra.repository.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.hyperglance.webcrawler.core.dto.CrawledUrl;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "CRAWLED_URL")
public class CrawledUrlEntity implements CrawledUrl<ParsedImageEntity>, Serializable {

    private static final long serialVersionUID = -5083749844511553198L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    //Unique to prevent re crawling of already crawled url
    @Column(name = "URL", nullable = false, unique = true)
    private String url;

    @Column(name = "COMPLETED", nullable = false)
    @JsonIgnore
    private Boolean completed;

    @Column(name = "DEPTH")
    @JsonIgnore
    private Integer depth;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "crawledUrl", fetch = FetchType.EAGER)
    @JsonProperty("images")
    @Fetch(value = FetchMode.SUBSELECT)
    private List<ParsedImageEntity> parsedImages;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    public Integer getDepth() {
        return depth;
    }

    public void setDepth(Integer depth) {
        this.depth = depth;
    }

    public List<ParsedImageEntity> getParsedImages() {
        return parsedImages;
    }

    public void setParsedImages(List<ParsedImageEntity> parsedImages) {
        this.parsedImages = parsedImages;
    }

}