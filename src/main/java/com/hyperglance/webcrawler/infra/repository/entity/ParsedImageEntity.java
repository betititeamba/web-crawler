package com.hyperglance.webcrawler.infra.repository.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hyperglance.webcrawler.core.dto.ParsedImage;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "PARSED_IMAGE")
public class ParsedImageEntity implements ParsedImage<CrawledUrlEntity> , Serializable {

    private static final long serialVersionUID = -557088040167432987L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    @Column(name = "IMAGE", nullable = false, unique = true)
    private String image;

    @ManyToOne
    @JoinColumn(name = "CRAWLED_URL_ID")
    @JsonIgnore
    private CrawledUrlEntity crawledUrl;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public CrawledUrlEntity getCrawledUrl() {
        return crawledUrl;
    }

    public void setCrawledUrl(CrawledUrlEntity crawledUrl) {
        this.crawledUrl = crawledUrl;
    }
}
