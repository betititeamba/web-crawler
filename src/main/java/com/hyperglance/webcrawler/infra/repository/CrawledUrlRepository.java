package com.hyperglance.webcrawler.infra.repository;

import com.hyperglance.webcrawler.core.dto.CrawledUrl;
import com.hyperglance.webcrawler.infra.repository.entity.CrawledUrlEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CrawledUrlRepository extends JpaRepository<CrawledUrlEntity, Long> {

    Optional<List<CrawledUrl>> findAllByCompletedFalse();
}
