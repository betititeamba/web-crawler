package com.hyperglance.webcrawler.infra.dao.impl;

import com.hyperglance.webcrawler.core.dao.CrawlerDao;
import com.hyperglance.webcrawler.core.dto.CrawledUrl;
import com.hyperglance.webcrawler.core.dto.ParsedImage;
import com.hyperglance.webcrawler.infra.repository.CrawledUrlRepository;
import com.hyperglance.webcrawler.infra.repository.ParsedImageRepository;
import com.hyperglance.webcrawler.infra.repository.entity.CrawledUrlEntity;
import com.hyperglance.webcrawler.infra.repository.entity.ParsedImageEntity;

import java.util.ArrayList;
import java.util.List;

public class CrawlerDaoImpl implements CrawlerDao {

    private final CrawledUrlRepository crawledUrlRepository;

    private final ParsedImageRepository parsedImageRepository;

    public CrawlerDaoImpl(CrawledUrlRepository crawledUrlRepository,
                          ParsedImageRepository parsedImageRepository) {
        this.crawledUrlRepository = crawledUrlRepository;
        this.parsedImageRepository = parsedImageRepository;
    }

    @Override
    public CrawledUrl newCrawledUrl(String url, int depth) {
        CrawledUrl crawledUrl = new CrawledUrlEntity();
        crawledUrl.setUrl(url);
        crawledUrl.setDepth(depth);
        crawledUrl.setCompleted(Boolean.FALSE);
        return crawledUrl;
    }

    @Override
    public CrawledUrl saveCrawledUrl(CrawledUrl crawledUrl) {
        return crawledUrlRepository.save((CrawledUrlEntity)crawledUrl);
    }

    @Override
    public ParsedImage newParsedImage(Long urlId, String image) {
        CrawledUrl crawledUrl = new CrawledUrlEntity();
        crawledUrl.setId(urlId);
        ParsedImage parsedImage = new ParsedImageEntity();
        parsedImage.setImage(image);
        parsedImage.setCrawledUrl(crawledUrl);
        return parsedImage;
    }

    @Override
    public ParsedImage saveParsedImage(ParsedImage parsedImage) {
        return parsedImageRepository.save((ParsedImageEntity)parsedImage);
    }

    @Override
    public void completeUrlParsing(Long urlId) {
        CrawledUrlEntity crawledUrl = crawledUrlRepository.findById(urlId).get();
        crawledUrl.setCompleted(Boolean.TRUE);
        crawledUrlRepository.save(crawledUrl);
    }

    @Override
    public List<? extends CrawledUrl> getCrawledUrls() {
        return crawledUrlRepository.findAll();
    }

    @Override
    public List<CrawledUrl> getUnfinishedCrawledUrls() {
        return crawledUrlRepository.findAllByCompletedFalse().orElse(new ArrayList<>());
    }
}
