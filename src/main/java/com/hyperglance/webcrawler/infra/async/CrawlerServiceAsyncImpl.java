package com.hyperglance.webcrawler.infra.async;

import com.hyperglance.webcrawler.core.service.CrawlerService;
import com.hyperglance.webcrawler.core.service.EntryService;
import com.hyperglance.webcrawler.core.service.ParserService;
import com.hyperglance.webcrawler.core.service.impl.CrawlerServiceImpl;
import com.hyperglance.webcrawler.core.util.LinkCycleChecker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class CrawlerServiceAsyncImpl extends CrawlerServiceImpl {

    @Autowired
    public CrawlerServiceAsyncImpl(CrawlerService crawlerService, ParserService parserService,
                                   EntryService entryService) {
        super(crawlerService, parserService, entryService);
    }

    @Override
    @Async
    public void crawl(String url, int dept, LinkCycleChecker checker) {
        super.crawl(url, dept, checker);
    }
}
