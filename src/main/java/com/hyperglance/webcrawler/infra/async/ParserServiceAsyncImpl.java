package com.hyperglance.webcrawler.infra.async;

import com.hyperglance.webcrawler.core.service.EntryService;
import com.hyperglance.webcrawler.core.service.impl.ParserServiceImpl;
import org.jsoup.nodes.Document;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class ParserServiceAsyncImpl extends ParserServiceImpl {

    public ParserServiceAsyncImpl(EntryService entryService) {
        super(entryService);
    }

    @Async
    @Override
    public void parseImages(Document document, String url, Long urlId) {
        super.parseImages(document, url, urlId);
    }
}
