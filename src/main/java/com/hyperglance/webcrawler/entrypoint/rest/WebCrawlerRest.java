package com.hyperglance.webcrawler.entrypoint.rest;

import com.hyperglance.webcrawler.core.util.LinkCycleChecker;
import com.hyperglance.webcrawler.entrypoint.dto.CrawlRequest;
import com.hyperglance.webcrawler.core.service.CrawlerService;
import com.hyperglance.webcrawler.core.service.EntryService;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/")
public class WebCrawlerRest {

    private final EntryService entryService;

    private final CrawlerService asyncCrawlerService;

    public WebCrawlerRest(EntryService entryService, CrawlerService asyncCrawlerService) {
        this.entryService = entryService;
        this.asyncCrawlerService = asyncCrawlerService;
    }

    @POST
    @Path("crawl")
    @Produces("application/json")
    @Consumes("application/json")
    public Response crawl(CrawlRequest crawlRequest) {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {

            }
        });
        asyncCrawlerService.crawl(crawlRequest.getUrl(), crawlRequest.getDepth(), new LinkCycleChecker());
        return Response.ok("crawling spawned").build();
    }

    @GET
    @Path("inventory")
    @Produces("application/json")
    public Response inventory() {
        return Response.ok(entryService.getCrawledUrls()).build();
    }

}
