package com.hyperglance.webcrawler.entrypoint.cmd;

import com.hyperglance.webcrawler.core.dto.CrawledUrl;
import com.hyperglance.webcrawler.core.service.CrawlerService;
import com.hyperglance.webcrawler.core.service.EntryService;
import com.hyperglance.webcrawler.core.util.LinkCycleChecker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Order(1)
public class CatchUpCrawl implements CommandLineRunner {

    private final EntryService entryService;

    private final CrawlerService crawlerService;

    @Autowired
    public CatchUpCrawl(EntryService entryService, CrawlerService crawlerService) {
        this.entryService = entryService;
        this.crawlerService = crawlerService;
    }

    @Override
    public void run(String... args) throws Exception {
        List<CrawledUrl> reprocessCrawls = this.entryService.getUnfinishedCrawledUrls();
        reprocessCrawls.stream().parallel().forEach(
                c -> {
                    crawlerService.crawl(c.getUrl(), c.getDepth(), new LinkCycleChecker());
                }
        );
    }
}
