package com.hyperglance.webcrawler.entrypoint.dto;

import java.io.Serializable;

public class CrawlRequest implements Serializable {

    private static final long serialVersionUID = -7102797242640395470L;

    private String url;

    private int depth;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }
}
