package com.hyperglance.webcrawler.core.dto;

import java.util.List;

public interface CrawledUrl<I extends ParsedImage> {
    Long getId();

    void setId(Long id);

    String getUrl();

    void setUrl(String url);

    Boolean getCompleted();

    void setCompleted(Boolean completed);

    Integer getDepth();

    void setDepth(Integer depth);

    List<I> getParsedImages();

    void setParsedImages(List<I> parsedImages);
}
