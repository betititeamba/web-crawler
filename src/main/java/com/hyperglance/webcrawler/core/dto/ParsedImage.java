package com.hyperglance.webcrawler.core.dto;

public interface ParsedImage<C extends CrawledUrl> {
    Long getId();

    void setId(Long id);

    String getImage();

    void setImage(String image);

    C getCrawledUrl() ;

    void setCrawledUrl(C crawledUrl);
}
