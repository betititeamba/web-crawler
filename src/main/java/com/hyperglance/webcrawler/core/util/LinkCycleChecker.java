package com.hyperglance.webcrawler.core.util;

import com.hyperglance.webcrawler.core.service.EntryService;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class LinkCycleChecker {
    private final Set<String> urls = Collections.synchronizedSet(new HashSet<>());

    public boolean check(String url) {
        return urls.add(url);
    }
}
