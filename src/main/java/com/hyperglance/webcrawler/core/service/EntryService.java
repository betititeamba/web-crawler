package com.hyperglance.webcrawler.core.service;

import com.hyperglance.webcrawler.core.dto.CrawledUrl;

import java.util.List;

public interface EntryService {
    Long addCrawledUrl(String url, int depth);

    Long addImageIntoUrl(Long urlId, String image);

    List<? extends CrawledUrl> getCrawledUrls();

    void completeUrlParsing(Long urlId);

    List<CrawledUrl> getUnfinishedCrawledUrls();

}
