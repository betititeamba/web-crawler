package com.hyperglance.webcrawler.core.service.impl;

import com.hyperglance.webcrawler.core.service.EntryService;
import com.hyperglance.webcrawler.core.service.ParserService;
import org.jsoup.helper.StringUtil;
import org.jsoup.nodes.Document;

public class ParserServiceImpl implements ParserService {

    private final EntryService entryService;

    public ParserServiceImpl(EntryService entryService) {
        this.entryService = entryService;
    }

    @Override
    public void parseImages(Document document, String url, Long urlId) {
        document.getElementsByTag("img").stream().forEach(e -> {
            String img = e.absUrl("src");
            if(!StringUtil.isBlank(img.trim())) {
                entryService.addImageIntoUrl(urlId,e.absUrl("src"));
            }
        });
        entryService.completeUrlParsing(urlId);
    }
}
