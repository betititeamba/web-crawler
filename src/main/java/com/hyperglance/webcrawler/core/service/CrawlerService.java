package com.hyperglance.webcrawler.core.service;

import com.hyperglance.webcrawler.core.util.LinkCycleChecker;

public interface CrawlerService {
    void crawl(String url, int dept, LinkCycleChecker checker);
}
