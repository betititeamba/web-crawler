package com.hyperglance.webcrawler.core.service;

import org.jsoup.nodes.Document;

public interface ParserService {
    void parseImages(Document document, String url, Long urlId);
}
