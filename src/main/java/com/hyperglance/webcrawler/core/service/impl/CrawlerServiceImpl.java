package com.hyperglance.webcrawler.core.service.impl;

import com.hyperglance.webcrawler.core.service.CrawlerService;
import com.hyperglance.webcrawler.core.service.EntryService;
import com.hyperglance.webcrawler.core.service.ParserService;
import com.hyperglance.webcrawler.core.util.LinkCycleChecker;
import org.jsoup.Jsoup;
import org.jsoup.helper.StringUtil;
import org.jsoup.nodes.Document;

import java.io.IOException;


public class CrawlerServiceImpl implements CrawlerService {

    private final CrawlerService crawlerService;

    private final ParserService parserService;

    private final EntryService entryService;

    public CrawlerServiceImpl(CrawlerService crawlerService, ParserService parserService,
                              EntryService entryService) {
        this.crawlerService = crawlerService;
        this.parserService = parserService;
        this.entryService = entryService;
    }

    @Override
    public void crawl(String url, int dept, LinkCycleChecker checker) {
        if(dept > 0 && checker.check(url)) {
            Document document = getDocument(url);
            Long urlId = entryService.addCrawledUrl(url,dept);
            parserService.parseImages(document, url, urlId);
            doCrawling(dept, document, checker);
        }
    }

    private Document getDocument(String url) {
        try {
            return Jsoup.connect(url).get();
        } catch (IOException e) {
            throw new IllegalArgumentException(String.format("Unable to connect to the given url : %s", url), e);
        }
    }

    private void doCrawling(int dept, Document document, LinkCycleChecker checker) {
        final int traverseDept = dept - 1;
        document.getElementsByTag("a").stream().forEach(a -> {
            String crawlUrl = a.absUrl("href");
            if (!StringUtil.isBlank(crawlUrl)) {
                crawlerService.crawl(crawlUrl, traverseDept, checker);
            }
        });
    }
}
