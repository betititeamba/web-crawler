package com.hyperglance.webcrawler.core.service.impl;

import com.hyperglance.webcrawler.core.dao.CrawlerDao;
import com.hyperglance.webcrawler.core.dto.CrawledUrl;
import com.hyperglance.webcrawler.core.service.EntryService;

import java.util.List;

public class EntryServiceImpl implements EntryService {

    private final CrawlerDao crawlerDao;

    public EntryServiceImpl(CrawlerDao crawlerDao) {
        this.crawlerDao = crawlerDao;
    }

    @Override
    public Long addCrawledUrl(String url, int depth) {
        return crawlerDao.saveCrawledUrl(crawlerDao.newCrawledUrl(url,depth)).getId();
    }

    @Override
    public Long addImageIntoUrl(Long urlId, String image) {
        return crawlerDao.saveParsedImage(crawlerDao.newParsedImage(urlId, image)).getId();
    }

    @Override
    public void completeUrlParsing(Long urlId) {
        crawlerDao.completeUrlParsing(urlId);
    }

    @Override
    public List<? extends CrawledUrl> getCrawledUrls() {
        return crawlerDao.getCrawledUrls();
    }

    @Override
    public List<CrawledUrl> getUnfinishedCrawledUrls() {
        return crawlerDao.getUnfinishedCrawledUrls();
    }
}
