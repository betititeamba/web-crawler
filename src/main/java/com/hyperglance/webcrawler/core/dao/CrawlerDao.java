package com.hyperglance.webcrawler.core.dao;

import com.hyperglance.webcrawler.core.dto.CrawledUrl;
import com.hyperglance.webcrawler.core.dto.ParsedImage;

import java.util.List;

public interface CrawlerDao {
    CrawledUrl newCrawledUrl(String url,int depth);

    CrawledUrl saveCrawledUrl(CrawledUrl crawledUrl);

    ParsedImage newParsedImage(Long urlId, String image);

    ParsedImage saveParsedImage(ParsedImage parsedImage);

    void completeUrlParsing(Long urlId);

    List<? extends CrawledUrl> getCrawledUrls();

    List<CrawledUrl> getUnfinishedCrawledUrls();
}
