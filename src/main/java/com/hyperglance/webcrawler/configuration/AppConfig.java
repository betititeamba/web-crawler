package com.hyperglance.webcrawler.configuration;

import com.hyperglance.webcrawler.core.dao.CrawlerDao;
import com.hyperglance.webcrawler.core.service.EntryService;
import com.hyperglance.webcrawler.core.service.impl.EntryServiceImpl;

import com.hyperglance.webcrawler.infra.dao.impl.CrawlerDaoImpl;
import com.hyperglance.webcrawler.infra.repository.CrawledUrlRepository;
import com.hyperglance.webcrawler.infra.repository.ParsedImageRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import javax.annotation.PreDestroy;
import java.util.concurrent.Executor;

@Configuration
@EnableAsync
public class AppConfig {

    @Bean
    CrawlerDao crawlerEntryDao(CrawledUrlRepository crawledUrlRepository,
                               ParsedImageRepository parsedImageRepository) {
        return new CrawlerDaoImpl(crawledUrlRepository, parsedImageRepository);
    }

    @Bean
    EntryService entryService(CrawlerDao crawlerDao) {
        return new EntryServiceImpl(crawlerDao);
    }

    @Bean
    public Executor taskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(2);
        executor.setMaxPoolSize(2);
        executor.setQueueCapacity(1000);
        executor.setThreadNamePrefix("WebCrawler-");
        executor.initialize();
        return executor;
    }
}
