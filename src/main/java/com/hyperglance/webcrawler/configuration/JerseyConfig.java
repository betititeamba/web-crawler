package com.hyperglance.webcrawler.configuration;

import com.hyperglance.webcrawler.entrypoint.rest.WebCrawlerRest;
import com.hyperglance.webcrawler.core.service.CrawlerService;
import com.hyperglance.webcrawler.core.service.EntryService;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JerseyConfig extends ResourceConfig {

    @Autowired
    public void registerWebCrawlerRest(EntryService entryService, CrawlerService asyncCrawlerService) {
        register(new WebCrawlerRest(entryService, asyncCrawlerService));
    }
}
